from django.contrib.gis.db import models
from django.db.models import UniqueConstraint
from django.utils.text import slugify
from django.contrib.auth.models import User
from lingua.fields import label_field, text_field


class Model(models.Model):
    class Meta:
        abstract = True

    id = models.AutoField(primary_key=True)
    created_at = models.DateTimeField(auto_now_add=True)


class Informed(Model):
    class Meta(Model.Meta):
        abstract = True

    title = label_field("Title")
    description = text_field("Description")

    def __str__(self):
        return str(self.title)


def collect_image_path(instance, filename):
    return f"collect/{instance.id}/image/{slugify(filename)}"


class Collect(Informed):
    DRAFT = "DR"
    PUBLISHED = "PU"
    STATUS = (
        (DRAFT, "Draft"),
        (PUBLISHED, "Published"),
    )
    status = models.CharField(max_length=2, choices=STATUS, default=DRAFT)
    image = models.ImageField(upload_to=collect_image_path, null=True, blank=True)
    main_question = label_field("Main Question", null=True, blank=True)
    short_description = text_field("Short Description")


class CollectCategory(Informed):
    collects = models.ManyToManyField(Collect, related_name="categories")


class Question(Informed):

    OBSERVER = "R"
    OBSERVED = "D"
    ON = (
        (OBSERVER, "Observer"),
        (OBSERVED, "Observed"),
    )
    kind = models.CharField(max_length=1, choices=ON)
    link = models.ManyToManyField(
        Collect,
        through="smartwater.QuestionLink",
        through_fields=("question", "collect"),
    )

    @property
    def collects(self):
        return [
            {
                "collect": link.collect_id,
                "order": link.order,
            }
            for link in self.link.through.objects.filter(question=self)
        ]


class QuestionLink(Model):
    class Meta:
        constraints = [
            UniqueConstraint(
                fields=["collect", "question"],
                name="question_link_constraint",
            )
        ]

    collect = models.ForeignKey(Collect, on_delete=models.CASCADE, related_name="+")
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name="+")
    order = models.IntegerField(default=1)


class Choice(Informed):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    order = models.IntegerField(default=1)


class Observation(Model):
    user = models.ForeignKey("auth.User", on_delete=models.CASCADE)
    session = models.CharField(max_length=40)
    collect = models.ForeignKey(Collect, on_delete=models.CASCADE)
    observed_point = models.PointField(srid=31370)

    def __str__(self):
        return "{}:{} #{}".format(self.collect, self.user, self.id)


class Answer(Model):
    observation = models.ForeignKey(Observation, on_delete=models.CASCADE)
    choice = models.ForeignKey(Choice, on_delete=models.CASCADE)

    def __str__(self):
        return "{} #{}".format(self.observation, self.choice)


class Annotation(Model):
    QUESTION = "question"
    CHOICE = "choice"
    OBSERVATION = "observation"
    ANSWER = "answer"
    ANNOTATION = "annotation"
    TARGET_TYPE = (
        (QUESTION, "Question"),
        (CHOICE, "Choice"),
        (OBSERVATION, "Observation"),
        (ANSWER, "Answer"),
        (ANNOTATION, "Annotation"),
    )
    target_type = models.CharField(max_length=12, choices=TARGET_TYPE)
    target_id = models.IntegerField()
    user = models.ForeignKey("auth.User", on_delete=models.CASCADE)
    collect = models.ForeignKey(Collect, on_delete=models.CASCADE)

    @staticmethod
    def get_target(target_type, target_id):
        models = {
            Annotation.QUESTION: Question,
            Annotation.CHOICE: Choice,
            Annotation.OBSERVATION: Observation,
            Annotation.ANSWER: Answer,
            Annotation.ANNOTATION: Annotation,
        }
        model = models[target_type]
        return model.objects.get(pk=target_id)


class TextAnnotation(Model):
    annotation = models.OneToOneField(
        Annotation, related_name="text", on_delete=models.CASCADE
    )
    text = models.TextField()


class ImageAnnotation(Model):
    annotation = models.OneToOneField(
        Annotation, related_name="image", on_delete=models.CASCADE
    )
    image = models.ForeignKey("documents.Image", on_delete=models.CASCADE)


class SoundAnnotation(Model):
    annotation = models.OneToOneField(
        Annotation, related_name="track", on_delete=models.CASCADE
    )
    track = models.ForeignKey("documents.Document", on_delete=models.CASCADE)


class Robot(Model):
    name = models.CharField(max_length=512)
    code = models.CharField(max_length=64)


class Community(Informed):
    collects = models.ManyToManyField(Collect, blank=True, null=True)
    robots = models.ManyToManyField(Robot, blank=True, null=True)
    geom = models.MultiPolygonField(dim=2, srid=31370)
    contact = models.ForeignKey(User, on_delete=models.PROTECT)
    short_description = text_field("Short Description")


class Affinity(Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="affinities")
    community = models.ForeignKey(Community, on_delete=models.CASCADE)


class UserSignupConfirmation(Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="+")
    token = models.UUIDField(editable=False, auto_created=True)
    visited = models.BooleanField(default=False)
