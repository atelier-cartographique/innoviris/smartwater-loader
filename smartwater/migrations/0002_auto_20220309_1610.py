# Generated by Django 3.2.6 on 2022-03-09 16:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('smartwater', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='collect',
            name='geom',
        ),
        migrations.AddField(
            model_name='community',
            name='short_description',
            field=models.CharField(default='short description', max_length=512),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='community',
            name='collects',
            field=models.ManyToManyField(blank=True, null=True, to='smartwater.Collect'),
        ),
        migrations.AlterField(
            model_name='community',
            name='robots',
            field=models.ManyToManyField(blank=True, null=True, to='smartwater.Robot'),
        ),
    ]
