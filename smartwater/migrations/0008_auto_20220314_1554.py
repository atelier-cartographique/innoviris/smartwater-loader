# Generated by Django 3.2.6 on 2022-03-14 15:54

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('smartwater', '0007_auto_20220314_1553'),
    ]

    operations = [
        migrations.RenameField(
            model_name='choice',
            old_name='description_intl',
            new_name='description',
        ),
        migrations.RenameField(
            model_name='choice',
            old_name='title_intl',
            new_name='title',
        ),
        migrations.RenameField(
            model_name='collect',
            old_name='description_intl',
            new_name='description',
        ),
        migrations.RenameField(
            model_name='collect',
            old_name='main_question_intl',
            new_name='main_question',
        ),
        migrations.RenameField(
            model_name='collect',
            old_name='short_description_intl',
            new_name='short_description',
        ),
        migrations.RenameField(
            model_name='collect',
            old_name='title_intl',
            new_name='title',
        ),
        migrations.RenameField(
            model_name='collectcategory',
            old_name='description_intl',
            new_name='description',
        ),
        migrations.RenameField(
            model_name='collectcategory',
            old_name='title_intl',
            new_name='title',
        ),
        migrations.RenameField(
            model_name='community',
            old_name='description_intl',
            new_name='description',
        ),
        migrations.RenameField(
            model_name='community',
            old_name='short_description_intl',
            new_name='short_description',
        ),
        migrations.RenameField(
            model_name='community',
            old_name='title_intl',
            new_name='title',
        ),
        migrations.RenameField(
            model_name='question',
            old_name='description_intl',
            new_name='description',
        ),
        migrations.RenameField(
            model_name='question',
            old_name='title_intl',
            new_name='title',
        ),
    ]
