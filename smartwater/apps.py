from django.apps import AppConfig


class SmartWaterConfig(AppConfig):
    name = "smartwater"
    label = "smartwater"
    verbose_name = "Smart Water"
    geodata = True

    def ready(self):
        from .signals import connect_signals
        connect_signals()
