from django.http import Http404
from django.urls import path, reverse

from .views import (
    SmarWaterPasswordResetView,
    SmartWaterResetConfirmView,
    delete_affinity,
    delete_answer,
    get_category_list,
    get_collect_list,
    get_communities_as_geojson,
    get_community_list,
    get_password_reset_done,
    get_question_list,
    get_choice_list,
    get_observation_list,
    get_answer_list,
    get_annotation_list,
    get_user_contributions,
    get_collect_as_geojson,
    post_affinity,
    post_observation,
    post_answer,
    post_annotation,
    post_register,
    update_observation,
    journal
)


def prefix(s):
    return f"smartwater{s}"


def collect_identifier(collect_id, request):
    rel_url = reverse(prefix(".get_collect_as_geojson"), args=[collect_id])
    if request is not None:
        return request.build_absolute_uri(rel_url)
    return rel_url


def communities_identifier(request):
    rel_url = reverse(prefix(".get_communities_as_geojson"))
    if request is not None:
        return request.build_absolute_uri(rel_url)
    return rel_url


def from_identifier(rid, request=None):
    schema = rid.hostname
    if schema == "collect":
        return collect_identifier(int(rid.path[1:]), request)
    elif schema == "communities":
        return communities_identifier(request)
    raise Http404()


urlpatterns = [
    path(
        prefix("/me/"), get_user_contributions, name=prefix(".get_user_contributions")
    ),
    path(prefix("/community/"), get_community_list, name=prefix(".get_community_list")),
    path(prefix("/collect/"), get_collect_list, name=prefix(".get_collect_list")),
    path(prefix("/question/"), get_question_list, name=prefix(".get_question_list")),
    path(prefix("/choice/"), get_choice_list, name=prefix(".get_choice_list")),
    path(prefix("/category/"), get_category_list, name=prefix(".get_category_list")),
    path(
        prefix("/observation/"),
        get_observation_list,
        name=prefix(".get_observation_list"),
    ),
    path(prefix("/answer/"), get_answer_list, name=prefix(".get_answer_list")),
    path(
        prefix("/annotation/<int:collect_id>/"),
        get_annotation_list,
        name=prefix(".get_annotation_list"),
    ),
    path(
        prefix("/geojson/collect/<int:collect_id>/"),
        get_collect_as_geojson,
        name=prefix(".get_collect_as_geojson"),
    ),
    path(
        prefix("/geojson/communities/"),
        get_communities_as_geojson,
        name=prefix(".get_communities_as_geojson"),
    ),
    # post
    path(prefix("/register/"), post_register, name=prefix(".post_register")),
    path(
        prefix("/observation/<int:collect_id>/<int:observation_id>"),
        update_observation,
        name=prefix(".update_observation"),
    ),
    path(
        prefix("/observation/<int:collect_id>"),
        post_observation,
        name=prefix(".post_observation"),
    ),
    path(
        prefix("/answer/<int:observation_id>/"),
        post_answer,
        name=prefix(".post_answer"),
    ),
    path(
        prefix("/answer/delete/<int:answer_id>/"),
        delete_answer,
        name=prefix(".delete_answer"),
    ),
    path(
        prefix("/affinity/<int:community_id>/"),
        post_affinity,
        name=prefix(".post_affinity"),
    ),
    path(
        prefix("/affinity/delete/<int:affinity_id>"),
        delete_affinity,
        name=prefix(".delete_affinity"),
    ),
    path(prefix("/annotation/"), post_annotation, name=prefix(".post_annotation")),
    # password reset
    path(
        prefix("/password-reset-done-<lang>.html"),
        get_password_reset_done,
        name=prefix(".get_password_reset_done"),
    ),
    path(
        prefix("/password-reset-<lang>.html"),
        SmarWaterPasswordResetView.as_view(),
        name=prefix(".password-reset"),
    ),
    path(
        prefix("/password-reset-confirm-<lang>/<uidb64>/<token>/"),
        SmartWaterResetConfirmView.as_view(),
        name=prefix(".password_reset_confirm"),
    ),
    path(
        prefix("/<int:community_id>/journal-<lang>.html"), journal, name=prefix('.journal')
    )

    # path(
    #     prefix("/password-reset-confirm-success/<user_id>/<token>/"),
    #     get_password_confirm_success,
    #     name=prefix(".get_password_confirm_success"),
    # ),
]
