from django.contrib import admin
from django.contrib.gis.admin.options import OSMGeoAdmin
from django.contrib.gis.forms import OSMWidget

from .models import (
    Collect,
    CollectCategory,
    Question,
    QuestionLink,
    Choice,
    Observation,
    Answer,
    Community,
    Robot,
)


class CollectAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "title",
    )


class QuestionLinkInline(admin.TabularInline):
    model = QuestionLink
    extra = 1


class QuestionAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "kind",
    )
    inlines = [QuestionLinkInline]


class ChoiceAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "question",
    )


class RobotAdmin(admin.ModelAdmin):
    list_display = ("name", "code")


class GeoWidget(OSMWidget):
    default_lon = 4.3623
    default_lat = 50.8383
    default_zoom = 8
    map_width = 1000
    map_height = 1000


class CommunityAdmin(OSMGeoAdmin):
    list_display = (
        "title",
        "description",
        "contact",
    )
    default_lon = 4.3623
    default_lat = 50.8383
    default_zoom = 11
    map_width = 800
    map_height = 800
    # wms_url = "https://geoservices-urbis.irisnet.be/geoserver/ows"
    # wms_layer = "Ortho2020"
    # gis_widget = GeoWidget


admin.site.register(Collect, CollectAdmin)
admin.site.register(CollectCategory)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Choice, ChoiceAdmin)
admin.site.register(Observation)
admin.site.register(Robot, RobotAdmin)
admin.site.register(Community, CommunityAdmin)
admin.site.register(Answer)
