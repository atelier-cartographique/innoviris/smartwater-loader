from multiprocessing import Pipe

# from multiprocessing.pool import ThreadPool
from threading import Thread, get_ident
from collections import namedtuple
import logging
from datetime import datetime
import time
from pyproj import Transformer
import math
import traceback

from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.mail import send_mail, mail_managers
from django.conf import settings
from django.template.loader import get_template
from django.core.cache import cache

from .models import Community, Observation

logger = logging.getLogger(__name__)

EMAIL_FROM = getattr(settings, "EMAIL_FROM", "default@example.com")

EMAIL_INTERVAL = getattr(settings, "EMAIL_INTERVAL", 1)

OBSERVATION_SUBJECT = "A new observation on SmartWater"
OBSERVATION_MAIL_TEMPLATE = get_template("smartwater/mail_observation.txt")

OBSERVATION_SUBJECT_CONTACT = "A new observation on SmartWater"
OBSERVATION_MAIL_CONTACT_TEMPLATE = get_template(
    "smartwater/mail_observation_contact.txt"
)

OBSERVATION_SUBJECT_ADMIN = "A new observation on SmartWater"
OBSERVATION_MAIL_ADMIN_TEMPLATE = get_template("smartwater/mail_observation_admin.txt")

MailUserCommand = namedtuple(
    "MailUserCommand", ["level", "subject", "body", "user_email"]  # 'user'
)

MailManagerCommand = namedtuple(
    "MailManagerCommand",
    [
        "level",  # 'manager'
        "subject",
        "body",
    ],
)

ObservationEvent = namedtuple("ObservationEvent", ["id"])
MailProcess = namedtuple("MailProcess", ["time"])


def mail_user(subject, body, user_email):
    try:
        send_mail(
            subject,
            body,
            EMAIL_FROM,
            [user_email],
            fail_silently=False,
        )
    except Exception as ex:
        logger.warning(f'Failed to send e-mail to {user_email}: "{ex}"')


transformer = Transformer.from_crs("EPSG:31370", "EPSG:4326")


def deg_to_dms(deg, type):
    decimals, number = math.modf(deg)
    d = int(number)
    m = int(decimals * 60)
    s = (deg - d - m / 60) * 3600.00
    compass = {"lat": ("N", "S"), "lon": ("E", "W")}
    compass_str = compass[type][0 if d >= 0 else 1]
    return "{}º{}'{:.2f}\"{}".format(abs(d), abs(m), abs(s), compass_str)


def point_to_dms(point):
    lat, lon = transformer.transform(*point.coords)
    lat, lon = deg_to_dms(lat, "lat"), deg_to_dms(lon, "lon")
    return f"{lat} {lon}"


def process_event(event: ObservationEvent):
    instance = Observation.objects.get(pk=event.id)
    creator = instance.user
    point = instance.observed_point
    communities = list(Community.objects.filter(geom__intersects=point))
    lat_lon = point_to_dms(point)

    users = {}
    for community in communities:
        for affinity in community.affinity_set.all():
            if affinity.user not in users:
                users[affinity.user] = []
            users[affinity.user].append(community)

    for u, cs in users.items():
        if u.id != creator.id and u.email is not None:
            content = OBSERVATION_MAIL_TEMPLATE.render(
                {
                    "user": u,
                    "communities": cs,
                    "observation": instance,
                    "multi": len(cs) > 1,
                    "lat_lon": lat_lon,
                }
            )
            mail_user(
                OBSERVATION_SUBJECT,
                content,
                u.email,
            )

    mail_managers(
        OBSERVATION_SUBJECT,
        OBSERVATION_MAIL_ADMIN_TEMPLATE.render(
            {
                "observation": instance,
                "communities": communities,
            }
        ),
    )

    for community in communities:
        if community.contact is not None and community.contact.email is not None:
            mail_user(
                OBSERVATION_SUBJECT,
                OBSERVATION_MAIL_CONTACT_TEMPLATE.render(
                    {
                        "contact": community.contact,
                        "observation": instance,
                        "community": community,
                    }
                ),
                community.contact.email,
            )


def mailer(conn):
    message_queue = []
    while True:
        try:
            msg = conn.recv()
            # print("mailer message", msg)
            if isinstance(msg, MailProcess):
                for item in message_queue:
                    process_event(item)
                message_queue = []

            elif isinstance(msg, ObservationEvent):
                message_queue.append(msg)
            else:
                logger.error("MailerError: unkowkn message type {}".format(type(msg)))
        except EOFError:
            logger.info("Stoped Mailer: end of pipe")
            return
        except Exception as ex:
            logger.error("MailerError: {}".format(ex))
            traceback.print_exc()


def timer(conn):
    try:
        while True:
            conn.send(MailProcess(datetime.now().timestamp()))
            time.sleep(60 * EMAIL_INTERVAL)
    except Exception:
        return


class MailBox:
    def __init__(self):
        rx, tx = Pipe(duplex=False)
        self._tx = tx
        self._process = Thread(target=mailer, args=(rx,), name="the-mailer")
        self._process.daemon = True
        self._process.start()

        self._timer = Thread(target=timer, args=(tx,), name="the-timer")
        self._timer.daemon = True
        self._timer.start()

    def send_obs(self, obs):
        self._tx.send(ObservationEvent(obs.id))


MAIL_BOX = None


def on_observation_save(sender, instance, created, **kwargs):
    """
    Send mail to users connected to the community of this observation
    """
    if created:
        MAIL_BOX.send_obs(instance)
        # creator = instance.user
        # point = instance.observed_point
        # communities = Community.objects.filter(geom__intersects=point)
        # users = {}
        # for community in communities:
        #     for affinity in  community.affinity_set.all():
        #         if affinity.user not in users:
        #             users[affinity.user] = []
        #         users[affinity.user].append(community)

        # for u, cs in users.items():
        #     if u.id != creator.id and u.email is not None:
        #         content = OBSERVATION_MAIL_TEMPLATE.render({
        #             "user": u,
        #             "communities": cs,
        #             "observation": instance
        #         })
        #         MAIL_BOX.send_user(
        #             OBSERVATION_SUBJECT,
        #             content,
        #             u.email,
        #         )

        # MAIL_BOX.send_managers(
        #     OBSERVATION_SUBJECT,
        #     OBSERVATION_MAIL_ADMIN_TEMPLATE.render({
        #         "observation": instance,
        #         "communities": communities,
        #     }),
        # )

        # for community in communities:
        #     if community.contact is not None and community.contact.email is not None:
        #         MAIL_BOX.send_user(
        #             OBSERVATION_SUBJECT,
        #             OBSERVATION_MAIL_CONTACT_TEMPLATE.render({
        #                 "contact": community.contact,
        #                 "observation": instance,
        #                 "communities": communities,
        #             }),
        #             community.contact.email
        #         )


def connect_signals():
    global MAIL_BOX
    if MAIL_BOX is None:
        MAIL_BOX = MailBox()
        post_save.connect(
            on_observation_save,
            sender="smartwater.Observation",
            dispatch_uid="smartwater_on_observation_save",
        )
