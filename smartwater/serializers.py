from django.contrib.gis.geos import GEOSGeometry
from django.contrib.auth.models import User
from django.template import loader
from json import loads as parse_json, dumps as stringify
from documents.models import Image, Document
from datetime import datetime

from .models import (
    Collect,
    CollectCategory,
    Community,
    Question,
    QuestionLink,
    Choice,
    Observation,
    Answer,
    Annotation,
    Robot,
    SoundAnnotation,
    TextAnnotation,
    ImageAnnotation,
    User,
    Affinity,
)


def _tojsts(dt):
    return int(dt.timestamp() * 1000)


def _fromjsts(ts):
    return datetime.fromtimestamp(ts / 1000)


def get_session_key(request):
    s = request.session
    if s.session_key is None:
        s.save()
    return s.session_key


class SerError(Exception):
    pass


def serialize_robot(robot: Robot):
    return {"id": robot.id, "name": robot.name, "code": robot.code}


def serialize_user(user: User):
    return {
        "id": user.pk,
        "name": user.get_full_name(),
        "email": user.email,
        "affinities": [serialize_affinity(a) for a in user.affinities.all()],
    }


def serialize_contact(contact: User):
    return {"id": contact.pk, "name": contact.get_full_name()}


def serialize_community(community: Community):
    return {
        "id": community.id,
        "name": community.title.to_dict(),
        "description": community.description.to_dict(),
        "shortDescription": community.short_description.to_dict(),
        "robots": [serialize_robot(r) for r in community.robots.all()],
        "collects": [c.id for c in community.collects.filter(status=Collect.PUBLISHED)],
        "contact": serialize_contact(community.contact),
        "geom": parse_json(community.geom.geojson),
    }


def serialize_collect(collect: Collect):
    try:
        image = collect.image.url
    except Exception:
        image = None
    return {
        "id": collect.id,
        "title": collect.title.to_dict(),
        "description": collect.description.to_dict(),
        "shortDescription": collect.short_description.to_dict(),
        "question": collect.main_question.to_dict(),
        "image": image,
        "categories": [c.id for c in collect.categories.all()],
        "timestamp": _tojsts(collect.created_at),
    }


def serialize_category(collect: CollectCategory):
    return {
        "id": collect.id,
        "title": collect.title.to_dict(),
        "description": collect.description.to_dict(),
        "timestamp": _tojsts(collect.created_at),
    }


def serialize_question_kind(question):
    if question.kind == Question.OBSERVED:
        return "observed"
    return "observer"


def serialize_question(question):
    kind = serialize_question_kind(question)
    id = question.id
    title = question.title
    description = question.description
    results = []

    for collect in question.collects:
        results.append(
            {
                "kind": kind,
                "id": id,
                "title": title.to_dict(),
                "description": description.to_dict(),
                "collect": collect["collect"],
                "order": collect["order"],
            }
        )

    return results


def serialize_choice(choice):
    return {
        "id": choice.id,
        "title": choice.title.to_dict(),
        "description": choice.description.to_dict(),
        "question": choice.question_id,
        "order": choice.order,
    }


# def serialize_point(p):
#     return {
#         'type': 'Point',
#         'coordinates': p.coords,
#     }


def serialize_observation(observation):
    tz = observation.created_at.tzinfo
    return {
        "id": observation.id,
        "user": observation.user_id,
        "collect": observation.collect_id,
        "timestamp": _tojsts(observation.created_at),
        "age": round((datetime.now(tz) - observation.created_at).total_seconds() / 86400),
        "observed": parse_json(observation.observed_point.geojson),
        "communities": [cid for cid in observation.communities if cid is not None],
    }


def serialize_answer(answer):
    return {
        "id": answer.id,
        "observation": answer.observation_id,
        "choice": answer.choice_id,
        "timestamp": _tojsts(answer.created_at),
    }


def serialize_affinity(affinity):
    return {
        "id": affinity.id,
        "user": affinity.user.id,
        "community": affinity.community.id,
    }


def serialize_annotation(annotation):
    text = getattr(annotation, "text", None)
    image = getattr(annotation, "image", None)
    track = getattr(annotation, "track", None)
    target = dict(kind=annotation.target_type, id=annotation.target_id)
    if text is not None:
        return {
            "id": annotation.id,
            "kind": "text",
            "collect": annotation.collect_id,
            "target": target,
            "user": annotation.user_id,
            "timestamp": _tojsts(annotation.created_at),
            "text": text.text,
        }

    elif image is not None:
        return {
            "id": annotation.id,
            "kind": "image",
            "collect": annotation.collect_id,
            "target": target,
            "user": annotation.user_id,
            "timestamp": _tojsts(annotation.created_at),
            "image": str(image.image_id),
        }

    elif track is not None:
        return {
            "id": annotation.id,
            "kind": "sound",
            "collect": annotation.collect_id,
            "target": target,
            "user": annotation.user_id,
            "timestamp": _tojsts(annotation.created_at),
            "track": str(track.track_id),
        }

    raise SerError("Empty Annotation")


def get_answer_for_link(answer_set, link):
    choices = {}
    try:
        answer = answer_set.get(choice__question=link.question)
        for lang in ("en", "fr", "nl"):
            question_name = getattr(link.question.title, lang, "--") + f" ({lang})"
            choice_name = getattr(answer.choice.title, lang, "--")
            choices[question_name] = choice_name
    except Exception:
        for lang in ("en", "fr", "nl"):
            question_name = getattr(link.question.title, lang, "--") + f" ({lang})"
            choices[question_name] = None
    return choices


def get_answer_for_link2(answer_set, link):

    try:
        answer = answer_set.get(choice__question=link.question)
        choice = {
            "question": {
                "id": link.question.id,
            },
            "answer": {
                "id": answer.choice.id,
            },
        }
        for lang in ("en", "fr", "nl"):
            question_name = getattr(link.question.title, lang, "--")
            choice_name = getattr(answer.choice.title, lang, "--")
            choice["question"][lang] = question_name
            choice["answer"][lang] = choice_name
    except Exception:
        choice = None
    return choice


def get_observation_html(o: Observation, lang):
    answers = Answer.objects.filter(observation=o.id)
    subelements = []
    for a in answers:
        choice = Choice.objects.filter(answer=a.id).first()
        question = choice.question
        annotations = Annotation.objects.filter(target_type="answer").filter(
            target_id=a.id
        )
        annotation_ser = []
        for ann in annotations:
            annotation_ser.append(serialize_annotation(ann))

        subelements.append(
            {"question": question, "choice": choice, "annotations": annotation_ser}
        )
    template_name = f"smartwater/journal-observation-{lang}.html"
    context = {
        "obs": {
            "user": o.user.first_name,
            "date": o.created_at,
            "questions": subelements,
        }
    }
    return loader.render_to_string(template_name, context)


def serialize_observation_as_feature(links, observation: Observation):
    props = {
        "created": observation.created_at.isoformat(),
        "questions": [
            get_answer_for_link2(observation.answer_set, link) for link in links
        ],
        "html_fr": get_observation_html(observation, "fr"),
        "html_nl": get_observation_html(observation, "nl"),
    }

    # we keep it here for compat with existing maps
    for link in links:
        props.update(get_answer_for_link(observation.answer_set, link))

    geometry = parse_json(observation.observed_point.geojson)
    geometry.update({"crs": {"type": "name", "properties": {"name": "epsg:31370"}}})

    return {
        "type": "Feature",
        "id": observation.id,
        "properties": props,
        "geometry": geometry,
    }


def serialize_collect_as_geojson(collect: Collect, extra_data={}, since=None):
    if since is not None:
        observations = Observation.objects.filter(collect=collect, created_at__gt=since)
    else:
        observations = Observation.objects.filter(collect=collect)
    links = QuestionLink.objects.filter(collect=collect)
    features = [
        serialize_observation_as_feature(links, o)
        for o in observations
        if o.answer_set.count() > 0
    ]

    return {
        "type": "FeatureCollection",
        "features": features,
        "info": extra_data,
    }


def serialize_community_as_feature(community: Community):
    props = {
        "id": community.id,
        "title_fr": getattr(community.title, "fr", "--"),
        "title_en": getattr(community.title, "en", "--"),
        "title_nl": getattr(community.title, "nl", "--"),
        "description_fr": getattr(community.description, "fr", "--"),
        "description_en": getattr(community.description, "en", "--"),
        "description_nl": getattr(community.description, "nl", "--"),
        "contact": community.contact.get_full_name(),
        "collects": [serialize_collect(co) for co in community.collects.all()],
    }
    geometry = parse_json(community.geom.geojson)
    geometry.update({"crs": {"type": "name", "properties": {"name": "epsg:31370"}}})
    return {
        "type": "Feature",
        "id": community.id,
        "properties": props,
        "geometry": geometry,
    }


def serialize_communities_as_geojson(communities):
    features = [serialize_community_as_feature(c) for c in communities if c.id > 1]

    return {
        "type": "FeatureCollection",
        "features": features,
    }


class DeSerError(Exception):
    pass


def deserialize_point(data):
    return GEOSGeometry(stringify(data))


def deserialize_observation(data, request):
    collect = Collect.objects.get(pk=data.pop("collect"))
    user = None if request.user.is_anonymous else request.user
    session = get_session_key(request)
    created_at = data.get("timestamp", None)
    if created_at is not None:
        created_at = _fromjsts(created_at)
    return Observation(
        collect=collect,
        user=user,
        session=session,
        created_at=created_at,
        observed_point=deserialize_point(
            data.pop("observed"),
        ),
    )


def deserialize_answer(data, observation):
    choice = Choice.objects.get(pk=data.pop("choice"))

    if observation.answer_set.filter(choice__question=choice.question_id).count() > 0:
        # raise DeSerError('Question already answered')
        observation.answer_set.filter(choice__question=choice.question_id).delete()

    if choice.question.link.filter(pk=observation.collect_id).count() == 0:
        raise DeSerError("Answer is not in the same Collect")

    return Answer(
        observation=observation,
        choice=choice,
    )


def deserialize_annotation(data, request):
    target = data.pop("target")
    target_type = target["kind"]
    target_id = target["id"]
    # here we just check that the target exists
    Annotation.get_target(target_type, target_id)
    kind = data.pop("kind")
    collect = Collect.objects.get(pk=data.pop("collect"))
    user = None if request.user.is_anonymous else request.user
    annotation = Annotation.objects.create(
        target_type=target_type,
        target_id=target_id,
        user=user,
        collect=collect,
    )

    if kind == "text":
        text = data.pop("text")
        TextAnnotation.objects.create(
            annotation=annotation,
            text=text,
        )
        return annotation

    elif kind == "image":
        image_id = data.pop("image")
        image = Image.objects.get(pk=image_id)
        ImageAnnotation.objects.create(
            annotation=annotation,
            image=image,
        )
        return annotation

    elif kind == "sound":
        track_id = data.pop("track")
        track = Document.objects.get(pk=track_id)
        SoundAnnotation.objects.create(
            annotation=annotation,
            track=track,
        )
        return annotation

    raise DeSerError("Unkown Annotation Kind")


def deserialize_registration(data):
    username = data.pop("username")
    email = data.pop("email")
    password = data.pop("password")
    community = Community.objects.get(pk=data.pop("community"))

    new_user = User(
        username=email,
        first_name=username,
        email=email,
        # is_active=False, To put back when signup e-mail confirmation works
    )
    new_user.set_password(password)
    new_user.save()
    Affinity.objects.create(user=new_user, community=community)

    return new_user
