from datetime import datetime
import itertools as it
from json import loads as parse_json
from django.db import IntegrityError
from django.shortcuts import get_object_or_404, render
from django.http import (
    JsonResponse,
    HttpResponseForbidden,
    HttpResponseServerError,
)
from django.urls import reverse
from django.views.decorators.http import require_http_methods
from django.contrib.auth.views import PasswordResetView, PasswordResetConfirmView
from django.contrib.auth import login
from django.views.decorators.clickjacking import xframe_options_exempt

from smartwater.serializers import (
    deserialize_annotation,
    deserialize_answer,
    deserialize_observation,
    deserialize_registration,
    get_session_key,
    serialize_affinity,
    serialize_annotation,
    serialize_answer,
    serialize_category,
    serialize_choice,
    serialize_collect,
    serialize_communities_as_geojson,
    serialize_community,
    serialize_observation,
    serialize_question,
    serialize_user,
    serialize_collect_as_geojson,
)
from smartwater.models import (
    Collect,
    CollectCategory,
    Community,
    Question,
    Choice,
    Observation,
    Answer,
    Annotation,
    Affinity,
)
from clients.views import ClientConfig


def observations_with_communities():
    sql = """SELECT o."id",
    o."created_at",
    o."user_id",
    o."session",
    o."collect_id",
    o."observed_point"::bytea,
    sum(a.count),
    array_agg(c.id) as communities
    FROM "smartwater_observation" as o
        left join smartwater_community as c on st_intersects(o.observed_point, c.geom)
        left join (select observation_id, count(*) as count from smartwater_answer group by observation_id) as a on o.id = a.observation_id
    where a.count is not null and a.count > 0 
    group by o.id, a.count
    order by o.created_at desc
"""
    return list(Observation.objects.raw(sql))


def base_list(serializer, queryset, flatten=False):
    if flatten:
        return JsonResponse(
            list(it.chain.from_iterable(map(serializer, queryset))), safe=False
        )
    return JsonResponse(list(map(serializer, queryset)), safe=False)


@require_http_methods(["GET", "OPTIONS"])
def get_collect_list(request):
    return base_list(serialize_collect, Collect.objects.all())


@require_http_methods(["GET", "OPTIONS"])
def get_question_list(request):
    return base_list(serialize_question, Question.objects.all(), flatten=True)


@require_http_methods(["GET", "OPTIONS"])
def get_choice_list(request):
    return base_list(serialize_choice, Choice.objects.all())


@require_http_methods(["GET", "OPTIONS"])
def get_observation_list(request):
    return base_list(serialize_observation, observations_with_communities())


@require_http_methods(["GET", "OPTIONS"])
def get_answer_list(request):
    return base_list(serialize_answer, Answer.objects.all())


@require_http_methods(["GET", "OPTIONS"])
def get_annotation_list(request, collect_id):
    collect = get_object_or_404(Collect, pk=collect_id)
    return base_list(serialize_annotation, Annotation.objects.filter(collect=collect))


@require_http_methods(["GET", "OPTIONS"])
def get_community_list(request):
    return base_list(serialize_community, Community.objects.all())


@require_http_methods(["GET", "OPTIONS"])
def get_category_list(request):
    return base_list(serialize_category, CollectCategory.objects.all())


@require_http_methods(["GET", "OPTIONS"])
def get_collect_as_geojson(request, collect_id):
    collect = get_object_or_404(Collect, pk=collect_id)
    collect_url = f"/client/smartwater/collect/1/{collect_id}"
    form_url = f"/client/smartwater/observation-form/1/{collect_id}"
    extra_data = {
        "form_url": form_url,
        "collect_url": collect_url,
        "form_name_fr": collect.title.fr,
        "form_name_nl": collect.title.nl,
        "form_name_en": collect.title.en,
    }
    since = None
    since_param = request.GET.get("since")
    if since_param is not None:
        try:
            since = datetime.fromisoformat(since_param)
        except ValueError:
            try:
                since = datetime.fromtimestamp(int(since_param) / 1000)
            except Exception:
                pass
    return JsonResponse(serialize_collect_as_geojson(collect, extra_data, since))


@require_http_methods(["GET", "OPTIONS"])
def get_communities_as_geojson(request):
    communties = Community.objects.all()
    return JsonResponse(serialize_communities_as_geojson(communties))


# def _get_collect_from_answer(answer):
#     return answer.choice.question.collect

# def render_observation(request, id):
#     observation = get_object_or_404(Observation, pk=id)
#     answers = observation.answer_set.all()
#     if answers.count() == 0:
#         return HttpResponseNotFound('Not Enough Answers')

#     collect = _get_collect_from_answer(answers[0])

#     data = {
#         'id': id,
#         'user': observation.user.id,
#         'collect': collect.id,
#         'answers': [serialize_answer(a) for a in answers],
#     }

#     return JsonResponse(data)


@require_http_methods(["GET", "OPTIONS"])
def get_user_contributions(request):
    if request.user.is_authenticated == False:
        return HttpResponseForbidden("One shall be authenticated to be recognised")

    user = request.user
    observations = list(
        map(
            serialize_observation,
            filter(lambda o: o.user_id == user.id, observations_with_communities()),
        )
    )
    annotations = list(
        map(
            serialize_annotation,
            Annotation.objects.filter(user=user).order_by("-created_at"),
        )
    )

    user = serialize_user(user)
    return JsonResponse(
        dict(user=user, observations=observations, annotations=annotations)
    )


@require_http_methods(["POST"])
def post_observation(request, collect_id):
    get_object_or_404(Collect, pk=collect_id)
    try:
        data = parse_json(request.body.decode("utf-8"))
        instance = deserialize_observation(data, request)
        instance.save()
        instance.communities = [
            c.id
            for c in Community.objects.filter(geom__intersects=instance.observed_point)
        ]
        return JsonResponse(serialize_observation(instance), status=201)
    except Exception as ex:
        return HttpResponseServerError("Error: {}".format(ex))


@require_http_methods(["POST", "PUT"])
def update_observation(request, collect_id, observation_id):
    collect = get_object_or_404(Collect, pk=collect_id)
    observation = get_object_or_404(Observation, pk=observation_id)

    if request.user.is_anonymous or observation.user.id != request.user.id:
        return HttpResponseForbidden("Not the observation author")

    if observation.collect.id != collect.id:
        raise ValueError("Wrong collect")

    try:
        data = parse_json(request.body.decode("utf-8"))
        object = deserialize_observation(data, request)
        observation.observed_point = object.observed_point
        observation.created_at = object.created_at
        observation.save()
        observation.communities = [
            c.id
            for c in Community.objects.filter(
                geom__intersects=observation.observed_point
            )
        ]
        return JsonResponse(serialize_observation(observation), status=200)
    except Exception as ex:
        return HttpResponseServerError("Error: {}".format(ex))


def check_instance_identity(request, instance):
    user = getattr(request, "user")

    # print('req.user>', user)
    # print('req.session>', get_session_key(request))
    # print('user auth>', observation.user == user)

    # print('obs.user>', observation.user)
    # print('obs.session>', observation.session)
    # print('session auth>', observation.session == get_session_key(request))

    if user.is_anonymous:
        return instance.session == get_session_key(request)
    else:
        return instance.user == user


@require_http_methods(["POST"])
def post_answer(request, observation_id):
    observation = get_object_or_404(Observation, pk=observation_id)

    if check_instance_identity(request, observation) is False:
        return HttpResponseForbidden("Answer is not from the observer")

    # # TODO: an edit mode? - pm
    # if observation.answer_set.filter(choice__question=question_id).count() > 0:
    #     return HttpResponseBadRequest('Question already answered')

    data = parse_json(request.body.decode("utf-8"))
    instance = deserialize_answer(data, observation)
    instance.save()
    return JsonResponse(serialize_answer(instance), status=201)


@require_http_methods(["DELETE"])
def delete_answer(request, answer_id):
    answer = get_object_or_404(Answer, pk=answer_id)

    if check_instance_identity(request, answer) is False:
        return HttpResponseForbidden("Answer is not from the observer")

    answer.delete()
    return JsonResponse({}, status=200)


@require_http_methods(["POST"])
def post_annotation(request):
    # try:
    data = parse_json(request.body.decode("utf-8"))
    instance = deserialize_annotation(data, request)
    return JsonResponse(serialize_annotation(instance), status=201)


@require_http_methods(["POST"])
def post_affinity(request, community_id):
    community = get_object_or_404(Community, pk=community_id)
    affinity = Affinity.objects.create(community=community, user=request.user)
    return JsonResponse(serialize_affinity(affinity), status=201)


@require_http_methods(["DELETE"])
def delete_affinity(request, affinity_id):
    affinity = get_object_or_404(Affinity, pk=affinity_id)

    if check_instance_identity(request, affinity) is False:
        return HttpResponseForbidden("Not your affinity")

    affinity.delete()
    return JsonResponse({}, status=200)


@require_http_methods(["POST"])
def post_register(request):
    try:
        data = parse_json(request.body.decode("utf-8"))
        user = deserialize_registration(data)
        user.backend = "django.contrib.auth.backends.ModelBackend"
        login(request, user)
        return JsonResponse({"tag": "ok", "id": user.id}, status=201)
    except IntegrityError:
        return JsonResponse({"tag": "error", "field": "email", "message": "duplicate"})
    except Exception as ex:
        return JsonResponse({"tag": "error", "field": "other", "message": "other"})
    # UserSignupConfirmation.objects.create(user=user)


## password reset


class SmarWaterPasswordResetView(PasswordResetView):
    title = "SmartReset"

    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)

        lang = kwargs.pop("lang")
        config = ClientConfig.get("smartwater")
        self.extra_context = {
            "lang": lang,
            "style_url": "clients/apps/" + config["version"]["style"],
        }
        self.extra_email_context = {
            "lang": lang,
        }
        self.template_name = f"smartwater/reset-password-{lang}.html"
        self.email_template_name = f"smartwater/reset-password-email-{lang}.html"
        self.success_url = reverse(
            "smartwater.get_password_reset_done", kwargs={"lang": lang}
        )


def get_password_reset_done(request, lang):
    template = f"smartwater/password-reset-done-{lang}.html"
    config = ClientConfig.get("smartwater")
    return render(
        request,
        template,
        {
            "lang": lang,
            "style_url": "clients/apps/" + config["version"]["style"],
        },
    )


class SmartWaterResetConfirmView(PasswordResetConfirmView):
    title = "SmartConfirm"
    post_reset_login = True

    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)

        lang = kwargs.pop("lang")
        config = ClientConfig.get("smartwater")
        self.extra_context = {
            "lang": lang,
            "style_url": "clients/apps/" + config["version"]["style"],
        }
        self.template_name = f"smartwater/reset-confirm-{lang}.html"
        self.success_url = "/client/smartwater"

    def get_user(self, uidb64):
        user = super().get_user(uidb64)
        if user is not None:
            user.backend = "django.contrib.auth.backends.ModelBackend"
        return user

@xframe_options_exempt
def journal(request, community_id, lang):
    config = ClientConfig.get("smartwater")
    template_name = f"smartwater/journal-{lang}.html"
    observations = filter(
        lambda obs: community_id in obs.communities, observations_with_communities()
    )
    elements = []
    for o in observations:
        answers = Answer.objects.filter(observation=o.id)
        subelements = []
        for a in answers:
            choice = Choice.objects.filter(answer=a.id).first()
            question = choice.question
            annotations = Annotation.objects.filter(target_type="answer").filter(
                target_id=a.id
            )
            annotation_ser = []
            for ann in annotations:
                annotation_ser.append(serialize_annotation(ann))

            subelements.append(
                {"question": question, "choice": choice, "annotations": annotation_ser}
            )
        elements.append(
            {
                "user": o.user.get_full_name(),
                "date": o.created_at,
                "questions": subelements,
            }
        )

    community = Community.objects.filter(id=community_id).first()
    context = {
        "lang": lang,
        "observations": elements,
        "community": community.title,
        "style_url": "clients/apps/" + config["version"]["style"],
    }
    return render(request, template_name, context)


# def get_password_confirm_success(request, user_id, token):
#     try:
#         user = User.objects.get(pk=user_id)
#     except Exception:
#         return redirect("/client/smartwater")

#     if default_token_generator.check_token(user, token):
#         login(request, user)

#     return redirect("/client/smartwater")
